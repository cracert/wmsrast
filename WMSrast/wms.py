#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Convert WMS to raster layers
"""

__author__      = "Robert Szczepanek"
__email__       = "robert@szczepanek.pl"


from owslib.wms import WebMapService
import sys
import os
from osgeo import gdal
import time

# check also
# http://geographika.co.uk/automated-wms-testing-with-python


def test_owslib():
    # https://github.com/geopython/OWSLib

    #wms = WebMapService('http://land.discomap.eea.europa.eu/arcgis/services/Land/CLC2012_Dyna_WM/MapServer/WMSServer', version='1.3.0')
    wms = WebMapService('http://land.discomap.eea.europa.eu/arcgis/services/Land/CLC2012_Dyna_WM/MapServer/WMSServer')
    print wms.identification.type
    print wms.identification.version
    print wms.identification.title
    print wms.identification.abstract

    print list(wms.contents)
    print wms['0'].title
    print wms['0'].crsOptions

    print [op.name for op in wms.operations]
    print wms.getOperationByName('GetMap').methods
    print wms.getOperationByName('GetMap').formatOptions

    epsg = ('EPSG:4326',
            'EPSG:3857',
            'EPSG:3035')
    xy_range = ((18, 54.5, 18.5, 55),
            (2061033, 7224379, 2093881, 7254086),
            (4877867.6518010711297393, 2865399.9049990619532764,
             5329630.3593624243512750, 3234342.1337457285262644)
            )

    region = 1

    img = wms.getmap(layers=['0'],
                    srs=epsg[region],
                    bbox=xy_range[region],
                    size=(600, 600),
                    #size=(4518, 3689),
                    format='image/png',
                    transparent=True)
    out = open('img/image3.png', 'wb')
    out.write(img.read())
    out.close()

"""
Data range for selected region EPSG:3035
100m
xmin, ymin
xmax, ymax
 4877867.6518010711297393,2865399.9049990619532764
 5329630.3593624243512750,3234342.1337457285262644

(4877867.6518010711297393,2865399.9049990619532764,
             5329630.3593624243512750,3234342.1337457285262644)

"""


def get_tile(wms, epsg, xmin, ymin, file_name):

    #wms = WebMapService('http://land.discomap.eea.europa.eu/arcgis/services/Land/CLC2012_Dyna_WM/MapServer/WMSServer')
    #epsg = 'EPSG:3035'
    #xmin = 4877867.6518010711297393
    #ymin = 2865399.9049990619532764

    resolution = 100
    px = 1000
    xy_range = (xmin, ymin,
             xmin + px * resolution, ymin + px * resolution)

    img = wms.getmap(layers=['0'],
                    srs=epsg,
                    bbox=xy_range,
                    size=(px, px),
                    format='image/tiff',
                    transparent=True)
    #out = open('img/image5.tif', 'wb')
    try:
        out = open(file_name, 'wb')
    except IOError:
        create_folder('img')
        out = open(file_name, 'wb')
    out.write(img.read())
    out.close()


def update_metadata(datasetname, xmin, ymax, xmax, ymin):
    ds = gdal.Open(datasetname, gdal.GA_Update)

    srs = 'PROJCS["ETRS89 / ETRS-LAEA",GEOGCS["ETRS89",DATUM["European_Terrestrial_Reference_System_1989",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6258"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4258"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],PROJECTION["Lambert_Azimuthal_Equal_Area"],PARAMETER["latitude_of_center",52],PARAMETER["longitude_of_center",10],PARAMETER["false_easting",4321000],PARAMETER["false_northing",3210000],AUTHORITY["EPSG","3035"],AXIS["X",EAST],AXIS["Y",NORTH]]'
    ds.SetProjection(srs)

    #4877867 2965399 4977867 2865399
    tile_size = 1000
    gt = [xmin, (xmax - xmin) / tile_size, 0,
          ymax, 0, (ymin - ymax) / tile_size]
    ds.SetGeoTransform(gt)
    ds = None


def get_scene():

    wms = WebMapService('http://land.discomap.eea.europa.eu/arcgis/services/Land/CLC2012_Dyna_WM/MapServer/WMSServer')
    epsg = 'EPSG:3035'
    xmin = 4880000.0
    ymin = 2860000.0
    tile_size = 100 * 1000  # resolution [m] * cells [#]

    for row in range(5):
        for column in range(5):
            file_name = 'img/clc2012-{}-{}.tif'.format(row, column)

            get_tile(wms,
                     epsg,
                     xmin + tile_size * column,
                     ymin + tile_size * row,
                     file_name)

            update_metadata(file_name,
                            xmin + tile_size * column,
                            ymin + tile_size * (row + 1),
                            xmin + tile_size * (column + 1),
                            ymin + tile_size * row)

            time.sleep(10)  # wait 10s


def create_folder(name):
    if not os.path.exists(name):
        os.makedirs(name)


if __name__ == '__main__':
    #test_owslib()
    #get_tile()
    #update_metadata("img/image5.tif", 4877867, 2965399, 4977867, 2865399)
    get_scene()



"""
https://svn.osgeo.org/gdal/trunk/gdal/swig/python/scripts/gdal_edit.py
ZWRACA BŁĄÐ

https://raw.githubusercontent.com/agrismart/gdal-1.9.2/master/swig/python/samples/gdal_edit.py
jako gdal_edit2.py

python gdal_edit2.py -a_srs PROJCS["ETRS89 / ETRS-LAEA",GEOGCS["ETRS89",DATUM["European_Terrestrial_Reference_System_1989",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6258"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4258"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],PROJECTION["Lambert_Azimuthal_Equal_Area"],PARAMETER["latitude_of_center",52],PARAMETER["longitude_of_center",10],PARAMETER["false_easting",4321000],PARAMETER["false_northing",3210000],AUTHORITY["EPSG","3035"],AXIS["X",EAST],AXIS["Y",NORTH]] -a_ullr 4877867 2965399 4977867 2865399 image4.tif

"""
